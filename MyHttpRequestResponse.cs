﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;


namespace CheckSites
{
    public class MyHttpRequestResponse
    {
        public int StatusCode { get; private set; }
        private string Url { get; set; }
        private static readonly HttpClient client = new HttpClient();


        public MyHttpRequestResponse(string url)
        {
            this.Url = url;
            RequestResponseSite(url);
        }

        public MyHttpRequestResponse(string url, string login, string pass)
        {
            this.Url = url;
            RequestResponseSite(url, login, pass);
        }

        public override string ToString()
        {
            return $"Code = {StatusCode} from {Url} ";
        }

        private void RequestResponseSite(String url)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                this.StatusCode = (int)response.StatusCode;
                response.Close();
            }
            catch (WebException ex)
            {
                if (ex.Response != null) { this.StatusCode = (int)((HttpWebResponse)ex.Response).StatusCode; }
                else { this.StatusCode = 10; }
            }
        }

        private async Task RequestResponseSite(string url, string login, string password)
        {
            var values = new Dictionary<string, string>
            {
                { "login", login },
                { "password", password }
            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync(url, content);

            var responseString = response.StatusCode;

            if (responseString == HttpStatusCode.InternalServerError)
            {
                responseString = HttpStatusCode.OK;
            }

            this.StatusCode = (int)responseString;
        }
    }
}
