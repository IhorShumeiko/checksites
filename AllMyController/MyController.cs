﻿using ChoETL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CheckSites.AllMyController
{
    public class MyController
    {
        private DateTime DateToday { get; set; }

        public void MyWriter(FailSite fs)
        {
            DateToday = DateTime.Now;
            var path = Application.StartupPath + $@".\Logs\Log_{DateToday.Day}_{DateToday.Month}_{DateToday.Year}" + ".txt";

            if (File.Exists(path))
            {
                try
                {
                    using (StreamWriter sw = File.AppendText(path))
                    {
                        sw.WriteLine(fs.ToString());
                        sw.Close();
                    }
                }
                catch (NotSupportedException ex)
                { MessageBox.Show("Exception!!!!  " + ex); }
            }
            else
            {
                try
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(fs.ToString());
                        sw.Close();
                    }
                }
                catch (NotSupportedException ex)
                { MessageBox.Show("Exception!!!!  " + ex); }
            }
        }

        private void WriterJson(List<FailSite> reportFailSite)
        {
            DateToday = DateTime.Now;
            var pathJson = Application.StartupPath + $@"\Log_Json_{DateToday.Day}_{DateToday.Month}_{DateToday.Year}" + ".json";
            Console.WriteLine("From my Method count List: " + reportFailSite.Count());
            try
            {
                if (File.Exists(pathJson))
                { File.WriteAllText(pathJson, JsonConvert.SerializeObject(reportFailSite.ToArray())); }
                else
                {
                    using (StreamWriter file = File.CreateText(pathJson))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        //serialize object directly into file stream
                        serializer.Serialize(file, JsonConvert.SerializeObject(RemoveOldReportFailSite(reportFailSite).ToArray()));
                    }
                }
            }
            catch (NotSupportedException ex)
            { MessageBox.Show("Exception!!!!  " + ex); }
        }

        private List<FailSite> RemoveOldReportFailSite(List<FailSite> rf)
        {
            if (rf.Count() > 1)
            {
                rf.Reverse();
                rf.RemoveRange(1, rf.Count() - 1);
            }
            return rf;
        }

        public void ChangeStatusSite(Site s)
        {
            if (s.Work) { s.Work = false; } else { s.Work = true; }
            s.StartStatus = DateTime.Now;
            s.LastTime = DateTime.Now;
        }

        public void ResetTimer(Site s)
        { s.LastTime = DateTime.Now; }

        private int idexOfArrayUrls(Site s)
        {
            int index = 0;
            for (int i = 0; i < ArrayUrls.numberUrl; i++)
            {
                if (s.Url.Equals(ArrayUrls.Urls[i]))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public void SendAlarmMassege(Site s)
        {
            if (idexOfArrayUrls(s) >= ArrayUrls.startIndexServices)
            {
                sendAlarmToBotMyCredit(s);
            }
            else
            {
                String urlForBot = ArrayUrls.urlSendToBot + "/Competitor/" + s.Code.ToString() + s.UrlForSender;
                new MyHttpRequestResponse(urlForBot);
            }
        }

        public void SendAlarmMassegeForDatabase(string dbUrl)
        {
            sendAlarmToBotMyCredit(dbUrl);
        }

        public void AddFailSite(Site s, List<FailSite> failSites, DataTable failSitesTabel, List<FailSite> reportFailSite)
        {
            failSites.Add(new FailSite(s.Url, s.Code, s.StartStatus, DateTime.Now));
            reportFailSite.Add(new FailSite(s.Url, s.Code, s.StartStatus, DateTime.Now));
            MyWriter(failSites.Last());
            WriterJson(reportFailSite);
            var deltaFailTime = DateTime.Now - s.StartStatus;
            failSitesTabel.Rows.Add(s.Url, s.Code, s.StartStatus, deltaFailTime.ToString().Substring(0, 8));
        }

        public void UpdateFailSite(Site s, List<FailSite> failSites, DataTable failSitesTabel, List<FailSite> reportFailSite)
        {
            foreach (FailSite fs in failSites)
            {
                if (fs.Url == s.Url)
                {
                    fs.FinishFail = DateTime.Now;
                    fs.DeltaTime = (fs.FinishFail - fs.StartFail).ToString().Substring(0, 8);
                    reportFailSite.Add(fs);
                    MyWriter(fs);
                    WriterJson(reportFailSite);
                    failSitesTabel.Rows.Add(s.Url, s.Code, s.StartStatus, fs.DeltaTime);
                    break;
                }
            }
        }

        public void DeleteFailSite(Site s, List<FailSite> failSites, DataTable failSitesTabel, List<FailSite> reportFailSite)
        {
            foreach (FailSite fs in failSites)
            {
                if (fs.Url == s.Url)
                {
                    fs.FinishFail = DateTime.Now;
                    fs.DeltaTime = (fs.FinishFail - fs.StartFail).ToString().Substring(0, 8);
                    fs.Code = s.Code;
                    reportFailSite.Add(fs);
                    MyWriter(fs);
                    WriterJson(reportFailSite);
                    failSites.Remove(fs);
                    failSitesTabel.Rows.Add(s.Url, s.Code, s.StartStatus, fs.DeltaTime);
                    s.StartStatus = DateTime.Now;
                    break;
                }
            }
        }

        public void UpdateTextBox(Site s, List<TextBox> servicesTextBoxes)
        {
            TextBox tb = servicesTextBoxes.Find(servicesTextBoxe => servicesTextBoxe.Name == s.UrlForSender.Remove(0, 1));
            if (tb != null) { _ = tb.Invoke(method: (MethodInvoker)delegate { tb.Text = s.ToString(); }); }
        }

        public void UpdateLabelColor(Site s, List<Label> servicesLabels)
        {
            Label lbl = servicesLabels.Find(servicesLabel => servicesLabel.Name == s.UrlForSender.Remove(0, 1));
            if (s.Work && lbl != null) { lbl.BackColor = System.Drawing.Color.Green; }
            if (!s.Work && lbl != null) { lbl.BackColor = System.Drawing.Color.Red; }
        }

        public void JsonToCsv(string PathJsonFile)
        {
            DateToday = DateTime.Now;
            var pathFileCSV = Application.StartupPath + $@"\Report_{DateToday.Day}_{DateToday.Month}_{DateToday.Year}" + ".csv";
            try
            {
                if (File.Exists(pathFileCSV))
                {
                    MessageBox.Show($"File {pathFileCSV} exists - delete or move it and try again!");
                }
                else
                {
                    using (var r = new ChoJSONReader(PathJsonFile))
                    {
                        using (var w = new ChoCSVWriter(pathFileCSV).WithFirstLineHeader())
                        { w.Write(r); }
                    }
                    MessageBox.Show("File.csv creat Ok!!!");
                }
            }
            catch (UnauthorizedAccessException ex)
            { MessageBox.Show("Exception!!!!  " + ex); }

        }

        public string LoadJson(string pathJsonFile)
        {
            string date;
            var path = Application.StartupPath + $@"\{pathJsonFile}";
            using (StreamReader dataJson = new StreamReader(path))
            { date = dataJson.ReadToEnd(); }
            return date;
        }

        private void sendAlarmToBotMyCredit(Site s)
        {
            JsonObjForSender jObj = new JsonObjForSender(s, ArrayUrls.chatIdBotMyCredit);
            var jsonContent = JsonConvert.SerializeObject(jObj);
            var baseUrlBot = ArrayUrls.urlSendToBot + "/json_value";
            MyGetPostRequestResponse rr = new MyGetPostRequestResponse(baseUrlBot, "POST", jsonContent);
            rr.MyGetResponse();
            //MessageBox.Show($"Result Status code: {rr.Status}" + $" Url: {baseUrlBot}"); // для отладки
        }

        private void sendAlarmToBotMyCredit(string url)
        {
            JsonObjForSender jObj = new JsonObjForSender(url, "500", url, ArrayUrls.chatIdBotMyCredit);
            var jsonContent = JsonConvert.SerializeObject(jObj);
            var baseUrlBot = ArrayUrls.urlSendToBot + "/json_value";
            MyGetPostRequestResponse rr = new MyGetPostRequestResponse(baseUrlBot, "POST", jsonContent);
            rr.MyGetResponse();
            //MessageBox.Show($"Result Status code: {rr.Status}" + $" Url: {baseUrlBot}"); // для отладки
        }

    }
}
