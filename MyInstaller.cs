﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CheckSites
{
    public class MyInstaller
    {

        public MyInstaller() {}

        public MyInstaller(List<Site> listSites, DataTable allSitesTabel, DataTable failSitesTabel)
        {
            InitializeSites(listSites);
            InitializeHeaderOfTabels(allSitesTabel, failSitesTabel);
        }
        private void InitializeSites(List<Site> listSites)
        {
            for (int i = 0; i < ArrayUrls.numberUrl; i++)
            { listSites.Add(new Site(ArrayUrls.Urls[i], ArrayUrls.UrlForSender[i])); }
        }

        private void InitializeHeaderOfTabels(DataTable allSitesTabel, DataTable failSitesTabel)
        {
            allSitesTabel.Columns.Add("Url", typeof(string));
            allSitesTabel.Columns.Add("Code", typeof(int));
            allSitesTabel.Columns.Add("Start", typeof(DateTime));
            allSitesTabel.Columns.Add("DeltaTime", typeof(string));

            failSitesTabel.Columns.Add("Url", typeof(string));
            failSitesTabel.Columns.Add("Code", typeof(int));
            failSitesTabel.Columns.Add("Start", typeof(DateTime));
            failSitesTabel.Columns.Add("DeltaTime", typeof(string));
        }
    }
}
