﻿using CheckSites.AllMyController;
using System;
using System.Windows.Forms;


namespace CheckSites.Reports
{
    public partial class ReportForm1 : Form
    {
        string PathJsonFile { get; set; }
        private MyController controller = new MyController();

        public ReportForm1()
        {
            InitializeComponent();
            buttonSendReport.Click += new EventHandler(buttonSendReport_Click);
            buttonConvertToExcel.Click += new EventHandler(buttonConvertToExcel_Click);
        }

        private void TextBoxPathJsonFile_TextChanged(object sender, EventArgs e)
        {
            PathJsonFile = textBoxPathJsonFile.Text;
        }

        private void buttonSendReport_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Your wont send Json to " + PathJsonFile + " ?");
            sendReport(controller.LoadJson(PathJsonFile));
        }

        private void sendReport(string jsonContent)
        {
            var baseUrlBot = ArrayUrls.urlSendToBot + "/pushJson";
            MyGetPostRequestResponse rr = new MyGetPostRequestResponse(baseUrlBot, "POST", jsonContent);
            rr.MyGetResponse();
            MessageBox.Show($"Result Status code: {rr.Status}" + $" Url: {baseUrlBot}");
        }

        private void buttonConvertToExcel_Click(object sender, EventArgs e)
        { 
            controller.JsonToCsv(PathJsonFile); 
        }

        

    }
}
