﻿namespace CheckSites.Reports
{
    partial class ReportForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPathJsonFile = new System.Windows.Forms.Label();
            this.buttonSendReport = new System.Windows.Forms.Button();
            this.textBoxPathJsonFile = new System.Windows.Forms.TextBox();
            this.buttonConvertToExcel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelPathJsonFile
            // 
            this.labelPathJsonFile.AutoSize = true;
            this.labelPathJsonFile.Location = new System.Drawing.Point(19, 18);
            this.labelPathJsonFile.Name = "labelPathJsonFile";
            this.labelPathJsonFile.Size = new System.Drawing.Size(55, 13);
            this.labelPathJsonFile.TabIndex = 0;
            this.labelPathJsonFile.Text = "Path From";
            // 
            // buttonSendReport
            // 
            this.buttonSendReport.Location = new System.Drawing.Point(38, 393);
            this.buttonSendReport.Name = "buttonSendReport";
            this.buttonSendReport.Size = new System.Drawing.Size(101, 45);
            this.buttonSendReport.TabIndex = 2;
            this.buttonSendReport.Text = "Send Report";
            this.buttonSendReport.UseVisualStyleBackColor = true;
            // 
            // textBoxPathJsonFile
            // 
            this.textBoxPathJsonFile.Location = new System.Drawing.Point(90, 15);
            this.textBoxPathJsonFile.Name = "textBoxPathJsonFile";
            this.textBoxPathJsonFile.Size = new System.Drawing.Size(423, 20);
            this.textBoxPathJsonFile.TabIndex = 3;
            this.textBoxPathJsonFile.TextChanged += new System.EventHandler(this.TextBoxPathJsonFile_TextChanged);
            // 
            // buttonConvertToExcel
            // 
            this.buttonConvertToExcel.Location = new System.Drawing.Point(164, 393);
            this.buttonConvertToExcel.Name = "buttonConvertToExcel";
            this.buttonConvertToExcel.Size = new System.Drawing.Size(100, 45);
            this.buttonConvertToExcel.TabIndex = 5;
            this.buttonConvertToExcel.Text = "Convert to Excel";
            this.buttonConvertToExcel.UseVisualStyleBackColor = true;
            // 
            // ReportForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonConvertToExcel);
            this.Controls.Add(this.textBoxPathJsonFile);
            this.Controls.Add(this.buttonSendReport);
            this.Controls.Add(this.labelPathJsonFile);
            this.Name = "ReportForm1";
            this.Text = "ReportForm1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPathJsonFile;
        private System.Windows.Forms.Button buttonSendReport;
        private System.Windows.Forms.TextBox textBoxPathJsonFile;
        private System.Windows.Forms.Button buttonConvertToExcel;
    }
}