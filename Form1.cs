﻿using CheckSites.AllMyController;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;
using Aspose.Cells.ExternalConnections;


namespace CheckSites
{
    public partial class Form1 : Form
    {
        private List<Site> allSites = new List<Site>();
        private List<FailSite> failSites = new List<FailSite>();
        public List<FailSite> reportFailSite = new List<FailSite>();
        static List<Label> servicesLabels = new List<Label>();
        static List<TextBox> servicesTextBoxes = new List<TextBox>();
        private DataTable allSitesTabel = new DataTable();
        private DataTable failSitesTabel = new DataTable();
        private MyController controller = new MyController();

        const int MINUTE_OF_REQUEST = 5;

        public Form1()
        {
            InitializeComponent();
            new MyInstaller(allSites, allSitesTabel, failSitesTabel);
            requestSites.Click += new EventHandler(requestSites_Click);
            indicators.Click += new EventHandler(Indicators_Click);
            reports.Click += new EventHandler(reports_Click);
        }

        private void reports_Click(object sender, EventArgs e)
        {
            Reports.ReportForm1 report = new Reports.ReportForm1();
            report.ShowDialog();
            report.Dispose();
        }

        private void requestSites_Click(object sender, EventArgs e)
        {
            MyRequestResponseSites();
            CheckFailSite();
            allSitesGridView.DataSource = allSitesTabel;
            failSitesGridView.DataSource = failSitesTabel;

            const int interval60Minutes = MINUTE_OF_REQUEST * 60000; // milliseconds
            System.Timers.Timer checkForTime = new System.Timers.Timer();
            checkForTime.Interval = interval60Minutes;
            checkForTime.Elapsed += OnTimedEventA;
            checkForTime.Enabled = true;
        }

        private void OnTimedEventA(Object source, System.Timers.ElapsedEventArgs e)
        {
            MyRequestResponseSites();
            CheckFailSite();
            allSitesGridView.DataSource = allSitesTabel;
            failSitesGridView.DataSource = failSitesTabel;
        }

        private void Indicators_Click(object sender, EventArgs e)
        {
             Process.Start(Application.StartupPath + @"\Support Indicators 1.2\WindowsFormsTestConnection.exe");
        }

        private void MyRequestResponseSites()
        {
            // Check Database
            if (!CheckDatatbaseConnection())
            {
                controller.SendAlarmMassegeForDatabase("Back database");
            }

            foreach (Site site in allSites)
            {
                if (site.Url.Contains("Login"))
                {
                    MyHttpRequestResponse rr = new MyHttpRequestResponse(site.Url, "+380503011979", "Qwerty123456");
                    site.Code = rr.StatusCode;
                }
                else
                {
                    MyHttpRequestResponse rr = new MyHttpRequestResponse(site.Url);
                    site.Code = rr.StatusCode;
                }

                if ((site.Code != 200 && site.Code != 201) && site.Work)
                {
                    controller.ChangeStatusSite(site);
                    controller.SendAlarmMassege(site);
                }

                if ((site.Code != 200 && site.Code != 201) && !site.Work)
                { controller.ResetTimer(site); }

                if ((site.Code == 200 || site.Code == 201) && !site.Work)
                { controller.ChangeStatusSite(site); }

                if ((site.Code == 200 || site.Code == 201) && site.Work)
                { controller.ResetTimer(site); } 

                controller.UpdateTextBox(site, servicesTextBoxes);
                controller.UpdateLabelColor(site, servicesLabels);
                Console.WriteLine(site.ToString() + " " + DateTime.Now);
                var deltaFailTime = DateTime.Now - site.StartStatus;
                allSitesTabel.Rows.Add(site.Url, site.Code, site.StartStatus, deltaFailTime.ToString().Substring(0, 8));
            }
        }

        private bool CheckDatatbaseConnection()
        {
            using (var l_oConnection = new SqlConnection(ArrayUrls.databaseConnectionString))
            {
                try
                {
                    l_oConnection.Open();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        private void CheckFailSite()
        {
            foreach (Site site in allSites)
            {
                if (site.Work && failSites.Exists(failSites => failSites.Url == site.Url))
                { controller.DeleteFailSite(site, failSites, failSitesTabel, reportFailSite); }

                if (!site.Work && failSites.Exists(failSites => failSites.Url == site.Url))
                { controller.UpdateFailSite(site, failSites, failSitesTabel, reportFailSite); }

                if (!site.Work && !failSites.Exists(failSites => failSites.Url == site.Url))
                { controller.AddFailSite(site, failSites, failSitesTabel, reportFailSite); }
            }
        }
    }
}
