﻿
using System.Windows.Forms;

namespace CheckSites
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.requestSites                   = new Button();
            this.indicators                     = new Button();
            this.reports                        = new Button();
            this.allSitesGridView               = new DataGridView();
            this.failSitesGridView              = new DataGridView();
            this.labelSMSSender                 = new Label();
            this.labelEmailSender               = new Label();
            this.labelMCSArtellence             = new Label();
            this.labelMCSBankId                 = new Label();
            this.labelMCBonusSystem             = new Label();
            this.labelMCMarketingAPI            = new Label();
            this.labelScoringDataStorageApi     = new Label();
            this.labelMCMarketingAdmin          = new Label();
            this.labelMobileApiProvider         = new Label();
            this.labelMCNotificationsEMAIL      = new Label();
            this.labelMCNotificationsSMS        = new Label();
            this.labelMCSReplicationApi         = new Label();
            this.labelChatBot                   = new Label();
            this.labelSiteMyCredit              = new Label();
            this.labelBkiApi = new Label();
            this.labelBackLoginApi = new Label();
            this.labelBackApi = new Label();

            this.textBoxSMS                     = new TextBox();
            this.textBoxEmail                   = new TextBox();
            this.textBoxMCSArtellence           = new TextBox();
            this.textBoxMCSBankId               = new TextBox();
            this.textBoxMCBonusSystem           = new TextBox();
            this.textBoxMCMarketingAPI          = new TextBox();
            this.textBoxMCMarketingAdmin        = new TextBox();
            this.textBoxMobileApiProvider       = new TextBox();
            this.textBoxMCNotificationsEMAIL    = new TextBox();
            this.textBoxMCNotificationsSMS      = new TextBox();
            this.textBoxMCSReplicationApi       = new TextBox();
            this.textBoxScoringDataStorageApi   = new TextBox();
            this.textBoxChatBot                 = new TextBox();
            this.textBoxSiteMyCredit            = new TextBox();
            this.textBoxBkiApi = new TextBox();
            this.textBoxBackApi = new TextBox();
            this.textBoxBackLoginApi = new TextBox();

            ((System.ComponentModel.ISupportInitialize)(this.allSitesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failSitesGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // buttionRequestSites
            // 
            this.requestSites.Location = new System.Drawing.Point(25, 720);
            this.requestSites.Name = "requestSites";
            this.requestSites.Size = new System.Drawing.Size(100, 40);
            this.requestSites.TabIndex = 4;
            this.requestSites.Text = "Request Sites";
            this.requestSites.UseVisualStyleBackColor = true;
            // 
            // buttionIndicators
            // 
            this.indicators.Location = new System.Drawing.Point(145, 720);
            this.indicators.Name = "indicators";
            this.indicators.Size = new System.Drawing.Size(100, 40);
            this.indicators.TabIndex = 31;
            this.indicators.Text = "Indicators";
            this.indicators.UseVisualStyleBackColor = true;
            // 
            // buttionReports
            // 
            this.reports.Location = new System.Drawing.Point(265, 720);
            this.reports.Name = "reports";
            this.reports.Size = new System.Drawing.Size(100, 40);
            this.reports.TabIndex = 32;
            this.reports.Text = "Report";
            this.reports.UseVisualStyleBackColor = true;
            // 
            // allSitesGridView
            // 
            this.allSitesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allSitesGridView.Location = new System.Drawing.Point(750, 25);
            this.allSitesGridView.Name = "allSitesGridView";
            this.allSitesGridView.Size = new System.Drawing.Size(600, 400);
            this.allSitesGridView.TabIndex = 5;
            // 
            // failSitesGridView
            // 
            this.failSitesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.failSitesGridView.Location = new System.Drawing.Point(750, 450);
            this.failSitesGridView.Name = "failSitesGridView";
            this.failSitesGridView.Size = new System.Drawing.Size(600, 200);
            this.failSitesGridView.TabIndex = 6;
            // 
            // labelSMSSender
            // 
            this.labelSMSSender.AutoSize = true;
            this.labelSMSSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelSMSSender.Location = new System.Drawing.Point(53, 20);
            this.labelSMSSender.Name = "NotificationsSMSSender";
            this.labelSMSSender.Size = new System.Drawing.Size(135, 18);
            this.labelSMSSender.TabIndex = 1;
            this.labelSMSSender.Text = "Notifications.SMS.Sender";
            servicesLabels.Add(this.labelSMSSender);
            // 
            // labelEmailSender
            // 
            this.labelEmailSender.AutoSize = true;
            this.labelEmailSender.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelEmailSender.Location = new System.Drawing.Point(53, 60);
            this.labelEmailSender.Name = "NotificationsEmailSender";
            this.labelEmailSender.Size = new System.Drawing.Size(141, 18);
            this.labelEmailSender.TabIndex = 2;
            this.labelEmailSender.Text = "Notifications.Email.Sender";
            servicesLabels.Add(this.labelEmailSender);
            // 
            // labelMCSArtellence
            // 
            this.labelMCSArtellence.AutoSize = true;
            this.labelMCSArtellence.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCSArtellence.Location = new System.Drawing.Point(53, 100);
            this.labelMCSArtellence.Name = "MCSArtellence";
            this.labelMCSArtellence.Size = new System.Drawing.Size(35, 13);
            this.labelMCSArtellence.TabIndex = 7;
            this.labelMCSArtellence.Text = "MCS.Artellence";
            servicesLabels.Add(this.labelMCSArtellence);
            // 
            // labelMCSBankId
            // 
            this.labelMCSBankId.AutoSize = true;
            this.labelMCSBankId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCSBankId.Location = new System.Drawing.Point(53, 140);
            this.labelMCSBankId.Name = "MCSBankId";
            this.labelMCSBankId.Size = new System.Drawing.Size(35, 13);
            this.labelMCSBankId.TabIndex = 8;
            this.labelMCSBankId.Text = "MCS.BankId";
            servicesLabels.Add(this.labelMCSBankId);
            // 
            // labelMCBonusSystem
            // 
            this.labelMCBonusSystem.AutoSize = true;
            this.labelMCBonusSystem.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCBonusSystem.Location = new System.Drawing.Point(53, 180);
            this.labelMCBonusSystem.Name = "MCBonusSystem";
            this.labelMCBonusSystem.Size = new System.Drawing.Size(35, 13);
            this.labelMCBonusSystem.TabIndex = 9;
            this.labelMCBonusSystem.Text = "MC.BonusSystem";
            servicesLabels.Add(this.labelMCBonusSystem);
            // 
            // labelMCMarketingAPI
            // 
            this.labelMCMarketingAPI.AutoSize = true;
            this.labelMCMarketingAPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCMarketingAPI.Location = new System.Drawing.Point(53, 220);
            this.labelMCMarketingAPI.Name = "MCMarketingAPI";
            this.labelMCMarketingAPI.Size = new System.Drawing.Size(35, 13);
            this.labelMCMarketingAPI.TabIndex = 10;
            this.labelMCMarketingAPI.Text = "MC.MarketingAPI";
            servicesLabels.Add(this.labelMCMarketingAPI);
            // 
            // labelMCMarketingAdmin
            // 
            this.labelMCMarketingAdmin.AutoSize = true;
            this.labelMCMarketingAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCMarketingAdmin.Location = new System.Drawing.Point(53, 260);
            this.labelMCMarketingAdmin.Name = "MCMarketingAdmin";
            this.labelMCMarketingAdmin.Size = new System.Drawing.Size(35, 13);
            this.labelMCMarketingAdmin.TabIndex = 11;
            this.labelMCMarketingAdmin.Text = "MC.MarketingAdmin";
            servicesLabels.Add(this.labelMCMarketingAdmin);
            // 
            // labelMobileApiProvider
            // 
            this.labelMobileApiProvider.AutoSize = true;
            this.labelMobileApiProvider.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMobileApiProvider.Location = new System.Drawing.Point(53, 300);
            this.labelMobileApiProvider.Name = "MobileApiProvider";
            this.labelMobileApiProvider.Size = new System.Drawing.Size(35, 13);
            this.labelMobileApiProvider.TabIndex = 12;
            this.labelMobileApiProvider.Text = "Mobile.Api.Provider";
            servicesLabels.Add(this.labelMobileApiProvider);
            // 
            // labelMCNotificationsEMAIL
            // 
            this.labelMCNotificationsEMAIL.AutoSize = true;
            this.labelMCNotificationsEMAIL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCNotificationsEMAIL.Location = new System.Drawing.Point(53, 340);
            this.labelMCNotificationsEMAIL.Name = "MCNotificationsEMAIL";
            this.labelMCNotificationsEMAIL.Size = new System.Drawing.Size(35, 13);
            this.labelMCNotificationsEMAIL.TabIndex = 13;
            this.labelMCNotificationsEMAIL.Text = "MC.Notifications.EMAIL";
            servicesLabels.Add(this.labelMCNotificationsEMAIL);
            // 
            // labelMCNotificationsSMS
            // 
            this.labelMCNotificationsSMS.AutoSize = true;
            this.labelMCNotificationsSMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCNotificationsSMS.Location = new System.Drawing.Point(53, 380);
            this.labelMCNotificationsSMS.Name = "MCNotificationsSMS";
            this.labelMCNotificationsSMS.Size = new System.Drawing.Size(35, 13);
            this.labelMCNotificationsSMS.TabIndex = 14;
            this.labelMCNotificationsSMS.Text = "MC.Notifications.SMS";
            servicesLabels.Add(this.labelMCNotificationsSMS);
            // 
            // labelMCSReplicationApi
            // 
            this.labelMCSReplicationApi.AutoSize = true;
            this.labelMCSReplicationApi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelMCSReplicationApi.Location = new System.Drawing.Point(53, 420);
            this.labelMCSReplicationApi.Name = "MCSReplicationApi";
            this.labelMCSReplicationApi.Size = new System.Drawing.Size(41, 13);
            this.labelMCSReplicationApi.TabIndex = 15;
            this.labelMCSReplicationApi.Text = "MCS.ReplicationApi";
            servicesLabels.Add(this.labelMCSReplicationApi);
            // 
            // labelScoringDataStorageApi
            // 
            this.labelScoringDataStorageApi.AutoSize = true;
            this.labelScoringDataStorageApi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelScoringDataStorageApi.Location = new System.Drawing.Point(53, 460);
            this.labelScoringDataStorageApi.Name = "ScoringDataStorageApi";
            this.labelScoringDataStorageApi.Size = new System.Drawing.Size(35, 13);
            this.labelScoringDataStorageApi.TabIndex = 16;
            this.labelScoringDataStorageApi.Text = "Scoring.Data.Storage.Api";
            servicesLabels.Add(this.labelScoringDataStorageApi);
            // 
            // labelChatBot
            // 
            this.labelChatBot.AutoSize = true;
            this.labelChatBot.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelChatBot.Location = new System.Drawing.Point(53, 500);
            this.labelChatBot.Name = "telegrammessage.herokuapp.com";
            this.labelChatBot.Size = new System.Drawing.Size(35, 13);
            this.labelChatBot.TabIndex = 28;
            this.labelChatBot.Text = "ChatBot";
            servicesLabels.Add(this.labelChatBot);
            // 
            // labelSiteMycredit
            // 
            this.labelSiteMyCredit.AutoSize = true;
            this.labelSiteMyCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelSiteMyCredit.Location = new System.Drawing.Point(53, 540);
            this.labelSiteMyCredit.Name = "mycredit.ua";
            this.labelSiteMyCredit.Size = new System.Drawing.Size(35, 13);
            this.labelSiteMyCredit.TabIndex = 30;
            this.labelSiteMyCredit.Text = "Site Mycredit";
            servicesLabels.Add(this.labelSiteMyCredit);
            // 
            // labelBkiApi
            // 
            this.labelBkiApi.AutoSize = true;
            this.labelBkiApi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelBkiApi.Location = new System.Drawing.Point(53, 580);
            this.labelBkiApi.Name = "BkiApi";
            this.labelBkiApi.Size = new System.Drawing.Size(35, 13);
            this.labelBkiApi.TabIndex = 34;
            this.labelBkiApi.Text = "BkiApi";
            servicesLabels.Add(this.labelBkiApi);

            //
            // labelBackApi
            //
            this.labelBackApi.AutoSize = true;
            this.labelBackApi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelBackApi.Location = new System.Drawing.Point(53, 620);
            this.labelBackApi.Name = "BackApi";
            this.labelBackApi.Size = new System.Drawing.Size(62, 18);
            this.labelBackApi.TabIndex = 35;
            this.labelBackApi.Text = "BackApi";
            servicesLabels.Add(this.labelBackApi);


            //
            // labelBackLoginApi
            // 
            this.labelBackLoginApi.AutoSize = true;
            this.labelBackLoginApi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelBackLoginApi.Location = new System.Drawing.Point(53, 660);
            this.labelBackLoginApi.Name = "BackLoginApi";
            this.labelBackLoginApi.Size = new System.Drawing.Size(62, 18);
            this.labelBackLoginApi.TabIndex = 35;
            this.labelBackLoginApi.Text = "BackLoginApi";
            servicesLabels.Add(this.labelBackLoginApi);

            // 
            // textBoxSMS
            // 
            this.textBoxSMS.Location = new System.Drawing.Point(246, 20);
            this.textBoxSMS.Name = "NotificationsSMSSender";
            this.textBoxSMS.Size = new System.Drawing.Size(400, 20);
            this.textBoxSMS.TabIndex = 0;
            servicesTextBoxes.Add(this.textBoxSMS);
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(246, 60);
            this.textBoxEmail.Name = "NotificationsEmailSender";
            this.textBoxEmail.Size = new System.Drawing.Size(400, 20);
            this.textBoxEmail.TabIndex = 3;
            servicesTextBoxes.Add(this.textBoxEmail);
            // 
            // textBoxMCSArtellence
            // 
            this.textBoxMCSArtellence.Location = new System.Drawing.Point(246, 100);
            this.textBoxMCSArtellence.Name = "MCSArtellence";
            this.textBoxMCSArtellence.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCSArtellence.TabIndex = 17;
            servicesTextBoxes.Add(this.textBoxMCSArtellence);
            // 
            // textBoxMCSBankId
            // 
            this.textBoxMCSBankId.Location = new System.Drawing.Point(246, 140);
            this.textBoxMCSBankId.Name = "MCSBankId";
            this.textBoxMCSBankId.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCSBankId.TabIndex = 18;
            servicesTextBoxes.Add(this.textBoxMCSBankId);
            // 
            // textBoxMCBonusSystem
            // 
            this.textBoxMCBonusSystem.Location = new System.Drawing.Point(246, 180);
            this.textBoxMCBonusSystem.Name = "MCBonusSystem";
            this.textBoxMCBonusSystem.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCBonusSystem.TabIndex = 19;
            servicesTextBoxes.Add(this.textBoxMCBonusSystem);
            // 
            // textBoxMCMarketingAPI
            // 
            this.textBoxMCMarketingAPI.Location = new System.Drawing.Point(246, 220);
            this.textBoxMCMarketingAPI.Name = "MCMarketingAPI";
            this.textBoxMCMarketingAPI.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCMarketingAPI.TabIndex = 20;
            servicesTextBoxes.Add(this.textBoxMCMarketingAPI);
            // 
            // textBoxMCMarketingAdmin
            // 
            this.textBoxMCMarketingAdmin.Location = new System.Drawing.Point(246, 260);
            this.textBoxMCMarketingAdmin.Name = "MCMarketingAdmin";
            this.textBoxMCMarketingAdmin.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCMarketingAdmin.TabIndex = 21;
            servicesTextBoxes.Add(this.textBoxMCMarketingAdmin);
            // 
            // textBoxMobileApiProvider
            // 
            this.textBoxMobileApiProvider.Location = new System.Drawing.Point(246, 300);
            this.textBoxMobileApiProvider.Name = "MobileApiProvider";
            this.textBoxMobileApiProvider.Size = new System.Drawing.Size(400, 20);
            this.textBoxMobileApiProvider.TabIndex = 22;
            servicesTextBoxes.Add(this.textBoxMobileApiProvider);
            // 
            // textBoxMCNotificationsEMAIL
            // 
            this.textBoxMCNotificationsEMAIL.Location = new System.Drawing.Point(246, 340);
            this.textBoxMCNotificationsEMAIL.Name = "MCNotificationsEMAIL";
            this.textBoxMCNotificationsEMAIL.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCNotificationsEMAIL.TabIndex = 23;
            servicesTextBoxes.Add(this.textBoxMCNotificationsEMAIL);
            // 
            // textBoxMCNotificationsSMS
            // 
            this.textBoxMCNotificationsSMS.Location = new System.Drawing.Point(246, 380);
            this.textBoxMCNotificationsSMS.Name = "MCNotificationsSMS";
            this.textBoxMCNotificationsSMS.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCNotificationsSMS.TabIndex = 24;
            servicesTextBoxes.Add(this.textBoxMCNotificationsSMS);
            // 
            // textBoxMCSReplicationApi
            // 
            this.textBoxMCSReplicationApi.Location = new System.Drawing.Point(246, 420);
            this.textBoxMCSReplicationApi.Name = "MCSReplicationApi";
            this.textBoxMCSReplicationApi.Size = new System.Drawing.Size(400, 20);
            this.textBoxMCSReplicationApi.TabIndex = 25;
            servicesTextBoxes.Add(this.textBoxMCSReplicationApi);
            // 
            // textBoxScoringDataStorageApi
            // 
            this.textBoxScoringDataStorageApi.Location = new System.Drawing.Point(246, 460);
            this.textBoxScoringDataStorageApi.Name = "ScoringDataStorageApi";
            this.textBoxScoringDataStorageApi.Size = new System.Drawing.Size(400, 20);
            this.textBoxScoringDataStorageApi.TabIndex = 26;
            servicesTextBoxes.Add(this.textBoxScoringDataStorageApi);
            // 
            // textBoxChatBot
            // 
            this.textBoxChatBot.Location = new System.Drawing.Point(246, 500);
            this.textBoxChatBot.Name = "telegrammessage.herokuapp.com";
            this.textBoxChatBot.Size = new System.Drawing.Size(400, 20);
            this.textBoxChatBot.TabIndex = 27;
            servicesTextBoxes.Add(this.textBoxChatBot);
            // 
            // textBoxSiteMycredit
            // 
            this.textBoxSiteMyCredit.Location = new System.Drawing.Point(246, 540);
            this.textBoxSiteMyCredit.Name = "mycredit.ua";
            this.textBoxSiteMyCredit.Size = new System.Drawing.Size(400, 20);
            this.textBoxSiteMyCredit.TabIndex = 29;
            servicesTextBoxes.Add(this.textBoxSiteMyCredit);
            // 
            // textBoxBkiApi
            // 
            this.textBoxBkiApi.Location = new System.Drawing.Point(246, 580);
            this.textBoxBkiApi.Name = "BkiApi";
            this.textBoxBkiApi.Size = new System.Drawing.Size(400, 20);
            this.textBoxBkiApi.TabIndex = 33;
            servicesTextBoxes.Add(this.textBoxBkiApi);

            // 
            // textBoxBackApi
            //
            this.textBoxBackApi.Location = new System.Drawing.Point(246, 620);
            this.textBoxBackApi.Name = "BackApi";
            this.textBoxBackApi.Size = new System.Drawing.Size(400, 20);
            this.textBoxBackApi.TabIndex = 34;
            servicesTextBoxes.Add(this.textBoxBackApi);


            // 
            // textBoxBackLoginApi
            // 
            this.textBoxBackLoginApi.Location = new System.Drawing.Point(246, 660);
            this.textBoxBackLoginApi.Name = "BackLoginApi";
            this.textBoxBackLoginApi.Size = new System.Drawing.Size(400, 20);
            this.textBoxBackLoginApi.TabIndex = 35;
            servicesTextBoxes.Add(this.textBoxBackLoginApi);


            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1500, 800);
            this.Controls.Add(this.textBoxScoringDataStorageApi);
            this.Controls.Add(this.textBoxMCSReplicationApi);
            this.Controls.Add(this.textBoxMCNotificationsSMS);
            this.Controls.Add(this.textBoxMCNotificationsEMAIL);
            this.Controls.Add(this.textBoxMobileApiProvider);
            this.Controls.Add(this.textBoxMCMarketingAdmin);
            this.Controls.Add(this.textBoxMCMarketingAPI);
            this.Controls.Add(this.textBoxMCBonusSystem);
            this.Controls.Add(this.textBoxMCSBankId);
            this.Controls.Add(this.textBoxMCSArtellence);
            this.Controls.Add(this.textBoxEmail);
            this.Controls.Add(this.textBoxSMS);
            this.Controls.Add(this.textBoxChatBot);
            this.Controls.Add(this.textBoxSiteMyCredit);
            this.Controls.Add(this.textBoxBkiApi);
            this.Controls.Add(this.textBoxBackApi);
            this.Controls.Add(this.textBoxBackLoginApi);

            this.Controls.Add(this.labelMCSReplicationApi);
            this.Controls.Add(this.labelMCNotificationsSMS);
            this.Controls.Add(this.labelMCNotificationsEMAIL);
            this.Controls.Add(this.labelMobileApiProvider);
            this.Controls.Add(this.labelMCMarketingAdmin);
            this.Controls.Add(this.labelScoringDataStorageApi);
            this.Controls.Add(this.labelMCMarketingAPI);
            this.Controls.Add(this.labelMCBonusSystem);
            this.Controls.Add(this.labelMCSBankId);
            this.Controls.Add(this.labelMCSArtellence);
            this.Controls.Add(this.labelEmailSender);
            this.Controls.Add(this.labelSMSSender);
            this.Controls.Add(this.labelChatBot);
            this.Controls.Add(this.labelSiteMyCredit);
            this.Controls.Add(this.labelBkiApi);
            this.Controls.Add(this.labelBackApi);
            this.Controls.Add(this.labelBackLoginApi);

            this.Controls.Add(this.failSitesGridView);
            this.Controls.Add(this.allSitesGridView);
            this.Controls.Add(this.requestSites);
            this.Controls.Add(this.indicators);
            this.Controls.Add(this.reports);
            this.Name = "Form1";
            this.Text = "Check Site";
            ((System.ComponentModel.ISupportInitialize)(this.allSitesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failSitesGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
       
        private Button requestSites;
        private Button indicators;
        private Button reports;
        private DataGridView allSitesGridView;
        private DataGridView failSitesGridView;
        private Label labelSMSSender;
        private Label labelEmailSender;
        private Label labelMCSArtellence;
        private Label labelMCSBankId;
        private Label labelMCBonusSystem;
        private Label labelMCMarketingAPI;
        private Label labelScoringDataStorageApi;
        private Label labelMCMarketingAdmin;
        private Label labelMobileApiProvider;
        private Label labelMCNotificationsEMAIL;
        private Label labelMCNotificationsSMS;
        private Label labelMCSReplicationApi;
        private Label labelChatBot;
        private Label labelSiteMyCredit;
        private Label labelBkiApi;
        private Label labelBackApi;
        private Label labelBackLoginApi;
        private TextBox textBoxSMS;
        private TextBox textBoxEmail;
        private TextBox textBoxMCSArtellence;
        private TextBox textBoxMCSBankId;
        private TextBox textBoxMCBonusSystem;
        private TextBox textBoxMCMarketingAPI;
        private TextBox textBoxMCMarketingAdmin;
        private TextBox textBoxMobileApiProvider;
        private TextBox textBoxMCNotificationsEMAIL;
        private TextBox textBoxMCNotificationsSMS;
        private TextBox textBoxMCSReplicationApi;
        private TextBox textBoxScoringDataStorageApi;
        private TextBox textBoxChatBot;
        private TextBox textBoxSiteMyCredit;
        private TextBox textBoxBkiApi;
        private TextBox textBoxBackApi;
        private TextBox textBoxBackLoginApi;
    }
}

