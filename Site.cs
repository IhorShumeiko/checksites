﻿using System;


namespace CheckSites
{
    public class Site
    {
        public string Url { get; set; }
        public string UrlForSender { get; set; }
        public Boolean Work { get; set; }
        public int Code { get; set; }
        public DateTime StartStatus { get; set; }
        public DateTime LastTime { get; set; }
        private TimeSpan deltaTime;
        public TimeSpan DeltaTime
        {
            get { return deltaTime; }
            set { deltaTime = LastTime - StartStatus; }
        }

        public Site(String url, String urlForSender)
        {
            this.Url = url;
            this.UrlForSender = urlForSender;
            this.StartStatus = DateTime.Now;
            this.LastTime = DateTime.Now;
            this.DeltaTime = LastTime - StartStatus;
            this.Work = true;
            this.Code = 0;
        }

        public override string ToString()
        { return $" Code = {Code}; work = {Work}; " + this.Url; }

    }
}
