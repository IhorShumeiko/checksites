﻿
namespace CheckSites
{
    public static class ArrayUrls
    {
        public static string urlSendToBot       = "https://telegrammessage.herokuapp.com/api";
        public static string chatIdBotMyCredit  = "-1001474700467";
        public const int     numberUrl          = 36;
        public const int     startIndexServices = 19;
        public const string databaseConnectionString =
            "Data Source=10.11.0.72;Initial Catalog=tkLender;Persist Security Info=True;User ID=dba;Password=M9Nc6U5OwKl6xg4VCmMGaet;MultipleActiveResultSets=True;Connection Timeout=90; Asynchronous Processing=true;;Application Name=RAPI_Prod;Min Pool Size=20;Max Pool Size=200;Connection Lifetime=90;Connection Timeout=10;";

        public static string[] Urls = new string[numberUrl] {
        "https://credit7.ua/",                                  //1
        "https://creditplus.ua/",                               //2
        "https://moneyveo.ua/",                                 //3
        "https://forzacredit.com.ua/",                          //4
        "https://www.ccloan.ua/",                               //5
        "https://kf.ua/",                                       //6
        "https://soscredit.ua/",                                //7
        "https://miloan.ua/",                                   //8
        "https://www.credify.com.ua/",                          //9
        "https://e-groshi.com/",                                //10
        "https://ecash.ua/",                                    //11
        "https://my.sgroshi.com.ua/",                           //12
        "https://alexcredit.ua/",                               //13
        "https://creditkasa.ua/",                               //14
        "https://globalcredit.ua/",                             //15
        "https://api.pozichka.ua/analytics/",                   //16
        "https://soscredit.ua/",                                //17
        "https://globalcredit.ua/page-data/uk/page-data.json",  //18
        "https://monto.com.ua/",                                //19

       "https://telegrammessage.herokuapp.com/",               //20
        "https://mycredit.ua/",                                 //21
        "http://10.11.0.11:8251/IsAlive",                       //22  'Production', Notifications.SMS.Sender
        "http://10.11.0.11:8250/IsAlive",                       //23  'Production', Notifications.Email.Sender
        "http://10.11.0.11:8075/IsAlive",                       //24  'Production', 'MCS.Artellence', 1)
        "http://10.11.0.11:9271/IsAlive",                       //25  'Production', 'MCS.BankId', 1)
        "http://10.11.0.11:8096/IsAlive",                       //26  'Production', 'MC.BonusSystem', 1)
        "http://10.11.0.11:9289/IsAlive",                       //27  'Production', 'MC.MarketingAPI', 1)
        "http://10.11.0.11:9293/IsAlive",                       //28  'Production', 'MC.MarketingAdmin', 1)
        //"http://10.11.0.11:9274/IsAlive",                       //29  'Production', 'MCS.WebSocketApp', 0)
        "http://10.11.0.11:5000/IsAlive",                       //30  'Production', 'TkLenderApiInterlayer (MobileApiProvider)', 1)
        "http://10.11.0.11:8103/IsAlive",                       //31  'Production', 'MC.Notifications.EMAIL', 1)
        "http://10.11.0.11:8093/IsAlive",                       //32  'Production', 'MC.Notifications.SMS', 1)
        //"http://10.11.0.11:9272/IsAlive",                       //33  'Production', 'MCS.RealTimeMBKI', 0)
        "http://10.11.0.11:9290/IsAlive",                       //34  'Production', 'MCS.ReplicationApi', 1)
        "http://10.11.0.11:9273/IsAlive",                        //35  'Production', 'ScoringDataStorageApi', 1)
        "http://10.11.0.30:8107/IsAlive",                        //36  'Production', 'BKI', 1)
        "http://10.11.0.30:9288/IsAlive",                        // 37 'Production', 'BackApi'
        "http://10.11.0.30:9288/Api/LoginCustomer"                         // 37 'Production', 'BackLoginApi'

        };

        public static string[] UrlForSender = new string[numberUrl] {
        "/credit7.ua",                                //1
        "/creditplus.ua",                             //2
        "/moneyveo.ua",                               //3
        "/forzacredit.com.ua",                        //4
        "/www.ccloan.ua",                             //5
        "/kf.ua",                                     //6
        "/soscredit.ua",                              //7
        "/miloan.ua",                                 //8
        "/www.credify.com.ua",                        //8
        "/e-groshi.com",                              //10
        "/ecash.ua",                                  //11
        "/my.sgroshi.com.ua",                         //12
        "/alexcredit.ua",                             //13
        "/creditkasa.ua",                             //14
        "/globalcredit.ua",                           //15
        "/api.pozichka.ua",                           //16
        "/soscredit.ua",                              //17
        "/globalcredit.ua",                           //18
        "/monto.com.ua",                              //19
       
        "/telegrammessage.herokuapp.com",             //20
        "/mycredit.ua",                               //21
        "/NotificationsSMSSender",                    //22  'Production', Notifications.SMS.Sender
        "/NotificationsEmailSender",                  //23  'Production', Notifications.Email.Sender
        "/MCSArtellence",                             //24  'Production', 'MCS.Artellence', 1)
        "/MCSBankId",                                 //25  'Production', 'MCS.BankId', 1)
        "/MCBonusSystem",                             //26  'Production', 'MC.BonusSystem', 1)
        "/MCMarketingAPI",                            //27  'Production', 'MC.MarketingAPI', 1)
        "/MCMarketingAdmin",                         //28  'Production', 'MC.MarketingAdmin', 1)
        //"/MCSWebSocketApp",                          //29  'Production', 'MCS.WebSocketApp', 0)
        "/MobileApiProvider",                         //30  'Production', 'TkLenderApiInterlayer (MobileApiProvider)', 1)
        "/MCNotificationsEMAIL",                    //31  'Production', 'MC.Notifications.EMAIL', 1)
        "/MCNotificationsSMS",                      //32  'Production', 'MC.Notifications.SMS', 1)
        //"/MCSRealTimeMBKI",                          //33  'Production', 'MCS.RealTimeMBKI', 0)
        "/MCSReplicationApi",                        //34  'Production', 'MCS.ReplicationApi', 1)
        "/ScoringDataStorageApi",                      //35  'Production', 'ScoringDataStorageApi', 1)
        "/BkiApi",                      //36  'Production', 'BKIApi', 1),
        "/BackApi",
        "/BackLoginApi"
        
        };

      
    }
}
