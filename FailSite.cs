﻿using System;

namespace CheckSites
{
    public class FailSite
    {
        public String Url { get; set; }
        public int Code { get; set; }

        public DateTime StartFail { get; set; }
        public DateTime FinishFail { get; set; }
        public string DeltaTime { get; set; }

        public FailSite(String url, int code, DateTime start, DateTime finish)
        {
            this.Url = url;
            this.Code = code;
            this.StartFail = start;
            this.FinishFail = finish;
            this.DeltaTime = (finish - start).ToString().Substring(0, 8);
        }

        public override string ToString()
        { return this.Url + $"; Code = {this.Code}; Start = {StartFail} PeriodFail = {DeltaTime}"; }
    }
}
