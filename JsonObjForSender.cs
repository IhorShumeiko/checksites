﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSites
{
    class JsonObjForSender
    {
        public string site_name;
        public string error;
        public string url;
        public string chat_id;

        public JsonObjForSender(Site s, string chatId)
        {
            this.site_name = s.UrlForSender;
            this.error = s.Code.ToString();
            this.url = s.Url;
            this.chat_id = chatId;
        }

        public JsonObjForSender(string siteName, string error, string url, string chatId)
        {
            this.site_name = siteName;
            this.error = error;
            this.url = url;
            this.chat_id = chatId;
        }

    }
}
