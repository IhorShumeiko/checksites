﻿using System;
using System.Collections.Generic;
using System.Data;
using CheckSites;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Site = CheckSites.Site;
//using MyController = CheckSites.AllMyController.MyController;
using System.Linq;
using CheckSites.AllMyController;

namespace TestCheckSites
{
    [TestClass]
    public class UnitTest1
    {
        private List<Site> allSites = new List<Site>();
        private List<FailSite> failSites = new List<FailSite>();
        private DataTable allSitesTabel = new DataTable();
        private DataTable failSitesTabel = new DataTable();
        private MyController controller = new MyController();
        private Site s;
        const int MINUTE_OF_REQUEST = 5;

        public void installSite()
        {
            s = new Site("https://mycredit.ua/", "/mycredit.ua") { StartStatus = DateTime.Now };
            s.DeltaTime = DateTime.Now - s.StartStatus;
        }

        [TestMethod]
        public void TestChangeStatusSite()
        {
            installSite();
            Assert.AreEqual(true, s.Work);
            controller.ChangeStatusSite(s);
            Assert.AreEqual(false, s.Work);
            controller.ChangeStatusSite(s);
            Assert.AreEqual(true, s.Work);
        }

        [TestMethod]
        public void TestAddFailSite()
        {
            installSite();
            s.Code = 500;
            new MyInstaller().InitializeHeaderOfTabels(allSitesTabel, failSitesTabel);
            int countFailSite = failSites.Count;
            int countFailTabel = failSitesTabel.Rows.Count;
            Assert.AreEqual(0, countFailSite);
            Assert.AreEqual(0, countFailTabel);
            //controller.AddFailSite(s, failSites, failSitesTabel);
            countFailSite = failSites.Count;
            countFailTabel = failSitesTabel.Rows.Count;
            Assert.AreEqual(1, countFailSite);
            Assert.AreEqual(1, countFailTabel);
        }

        [TestMethod]
        public void TestUpdateFailSite()
        {
            TestAddFailSite();

            //controller.UpdateFailSite(s, failSites, failSitesTabel);
            int countFailSite = failSites.Count;
            int countFailTabel = failSitesTabel.Rows.Count;
            Assert.AreEqual(1, countFailSite);
            Assert.AreEqual(2, countFailTabel);
        }

        [TestMethod]
        public void TestDeleteFailSite()
        {
            TestAddFailSite();

            //controller.DeleteFailSite(s, failSites, failSitesTabel);
            int countFailSite = failSites.Count;
            int countFailTabel = failSitesTabel.Rows.Count;
            Assert.AreEqual(0, countFailSite);
            Assert.AreEqual(2, countFailTabel);
        }
    }
}
